import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigType } from '@nestjs/config';

import { AuthService } from './services/auth/auth.service';
import { UsersModule } from 'src/users/users.module';
import { LocalStategy } from './strategies/local.strategy';
import { JWTStrategy } from './strategies/jwt.strategy';
import { AuthController } from './controllers/auth/auth.controller';
import config from 'src/config';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.registerAsync({
      inject: [config.KEY],
      useFactory: (configService: ConfigType<typeof config>) => {
        return {
          secret: configService.jwtSecret,
          signOptions: { expiresIn: '10d' },
        };
      },
    }),
  ],
  providers: [AuthService, LocalStategy, JWTStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
