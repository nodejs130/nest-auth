import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { MongoIdPipe } from 'src/common/mongo-id/mongo-id.pipe';
import {
  AddProductToOrderDto,
  CreateOrderDto,
  UpdateOrderDto,
} from 'src/users/dtos/order.dto';
import { OrdersService } from 'src/users/services/orders/orders.service';

@Controller('orders')
export class OrdersController {
  constructor(private orderService: OrdersService) {}

  @Get()
  findAll() {
    return this.orderService.findAll();
  }

  @Get(':id')
  get(@Param('id', MongoIdPipe) id: string) {
    return this.orderService.findOne(id);
  }

  @Post()
  create(@Body() payload: CreateOrderDto) {
    return this.orderService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', MongoIdPipe) id: string,
    @Body() payload: UpdateOrderDto,
  ) {
    return this.orderService.update(id, payload);
  }

  @Delete(':id')
  delete(@Param('id', MongoIdPipe) id: string) {
    return this.orderService.remove(id);
  }

  @Put(':id/products')
  addProduct(
    @Param('id', MongoIdPipe) id: string,
    @Body() payload: AddProductToOrderDto,
  ) {
    return this.orderService.addProduct(id, payload.productId);
  }

  @Delete(':id/product/:productId')
  removeProdcut(
    @Param('id', MongoIdPipe) id: string,
    @Param('productId', MongoIdPipe) productId: string,
  ) {
    return this.orderService.removeProdcut(id, productId);
  }
}
